// addressRoutes.js

var express = require('express');
var app = express();
var addressRoutes = express.Router();

// Require Item model in our routes module
var Address = require('../models/Address');

// Defined store route
addressRoutes.route('/add').post(function (req, res) {
	var address = new Address(req.body);
	address.save()
    .then(item => {
    	res.status(200).json({'address': 'Address added successfully'});
    })
    .catch(err => {
    	res.status(400).send("unable to save to database");
    });
});

// Defined get data(index or listing) route
addressRoutes.route('/').get(function (req, res) {
	Address.find(function (err, addresss){
		if(err){
			console.log(err);
		}
		else {
			res.json(addresss);
		}
	});
});

// Defined edit route
addressRoutes.route('/edit/:id').get(function (req, res) {
	var id = req.params.id;
	Address.findById(id, function (err, address){
		res.json(address);
	});
});

//  Defined update route
addressRoutes.route('/update/:id').post(function (req, res) {
	Address.findById(req.params.id, function(err, address) {
		if (!address)
			return next(new Error('Could not load a Address Document using id ' + req.params.id));
		else {
            address.street = req.body.street;
            address.city = req.body.city;
            address.state = req.body.state;
            address.zipCode = req.body.zipCode;

			address.save().then(address => {
				res.json('Update complete');
			})
			.catch(err => {
				res.status(400).send("unable to update the database");
			});
		}
	});
});

// Defined delete | remove | destroy route
addressRoutes.route('/delete/:id').get(function (req, res) {
   Address.findOneAndDelete({_id: req.params.id}, function(err, address){
        if(err) res.json(err);
        else res.json('Successfully removed ' + Address + ' using id ' + req.params.id );
    });
});

module.exports = addressRoutes;